#ifndef thread_pool_
#define thread_pool_ 
#include <thread>
#include <functional>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <future>
#include <vector>
#include <atomic>
#include <type_traits>
#include <iostream>
#include <stdexcept>

using function_t = std::function<void()>;

class task_queue {
  public:
    bool blocking_pop(function_t& fun) {
      std::unique_lock<std::mutex> queue_lock{mtx_};
      while ( tasks_.empty() ) {
        if ( stopped_ ) return false;
        cv_.wait(queue_lock);
      }
      fun = std::move(tasks_.front());
      tasks_.pop();
      return true;
    }

    template<typename F>
      void push(F&& f) {
        {
          std::unique_lock<std::mutex> queue_lock{mtx_};
          tasks_.emplace(std::forward<F>(f));
        }
        cv_.notify_one();
      }

    void stop() {
      {
        std::unique_lock<std::mutex> queue_lock{mtx_};
        stopped_ = true;
      }
      cv_.notify_all();
    }

  private:
    std::queue<function_t> tasks_;
    std::mutex mtx_;
    std::condition_variable cv_;
    bool stopped_ = false;
};

class thread_pool {
  public:
    thread_pool(size_t th_num = std::thread::hardware_concurrency()) {
      tasks_collections_ = std::vector<task_queue>(th_num);
      for(int i = 0; i < th_num; ++i) {
        threads_.emplace_back( std::thread{ [this, i](){ run(i); }});
      }
    }

    ~thread_pool() {
      for(auto &t : tasks_collections_)
        t.stop();
      for(auto &t : threads_ ) 
        t.join();
    }

    template<typename T>
      void async(T&& fun) {
        tasks_collections_[get_task_counter()].push(std::forward<T>(fun));
      }

  private:
    void run(const int task_queue_index) {
      while(true) {
        function_t fun;
        if( !tasks_collections_[task_queue_index].blocking_pop(fun) )
          return;
        fun();
      }
    }

    int get_task_counter() {
      if (task_counter == tasks_collections_.size())
        return task_counter = 0;
      else return task_counter++;
    }

    int task_counter = 0;
    std::vector<std::thread> threads_;
    std::vector<task_queue> tasks_collections_;
};

#endif /* ifndef thread_pool_ */

